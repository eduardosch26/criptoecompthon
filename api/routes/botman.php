<?php
use App\Http\Controllers\BotManController;
use App\Conversation\LenoBot;
use Mpociot\BotMan\Messages\Message;
$botman = resolve('botman');

$botman->hears('/start', BotManController::class.'@cripto');
$botman->hears('/conversao', BotManController::class.'@conversao');
$botman->hears('/comprar', BotmanController::class.'@comprar');
$botman->hears('/help', function ($bot) {
    $bot->reply('Para criar sua moeda digite: /start');
    $bot->reply('Para saber a conversão da sua moeda para Reais digite: /conversao');
    $bot->reply('Para comprar a moeda de outro ecomper digite: /comprar');
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');
