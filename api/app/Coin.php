<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $fillable = ['rank', 'name', 'price', "marketCap", 'quantity', 'volume', 'variation'];
}
