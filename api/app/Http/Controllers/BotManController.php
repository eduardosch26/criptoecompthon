<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use App\Conversations\LenoBot;
use App\Conversations\Conversao;
use App\Conversations\Comprar;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    public function cripto(BotMan $bot)
    {
        $bot->startConversation(new LenoBot());
    }

    public function conversao(BotMan $bot)
    {
        $bot->startConversation(new Conversao());
    }

    public function comprar(BotMan $bot)
    {
        $bot->startConversation(new Comprar());
    }
}
