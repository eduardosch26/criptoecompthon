<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Coin;
use App\Moeda;
class CoinController extends Controller
{
    public function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:20',
            'marketCap' => 'nullable',
            'quantity' => 'nullable',
            'volume' => 'nullable',
            'variation' => 'nullable',
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $lower = strtolower($request->name);
        $nome = $this->tirarAcentos($lower);
        $id = 0;
        $id1 = 0;

        for($i=0; $i <strlen($nome); $i++)
        {
            $ascii = ord($nome[$i]) -96;
            $id = $id + $ascii;
        }

        for($i=0; $i <strlen($nome); $i++)
        {
            $ascii = 27-(ord($nome[$i])-96);
            $id1 = $id1 + $ascii;
        }
        

        $moeda = json_decode(@file_get_contents('https://api.coinlore.net/api/ticker/?id='. urlencode($id)))[0];
        $moeda1 = json_decode(@file_get_contents('https://api.coinlore.net/api/ticker/?id='. urlencode($id1)))[0];

        $media = (floatval($moeda->price_usd) + floatval($moeda1->price_usd))/2;

        $coin = Coin::create([
            'name' => $request->name,
            'rank' => $request->rank,
            'marketCap' => $request->marketCap,
            'quantity' => $request->quantity,
            'volume' => $request->volume,
            'variation' => $request->variation,
            'price' => $media,
        ]);



        return response()->json($coin);

    }
}
