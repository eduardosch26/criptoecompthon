<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use App\Moeda;
class LenoBot extends Conversation
{
    public function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

    public function Pergunta()
    {
        $question = Question::create("Digite o nome da sua Cripto Moeda: ");

        return $this->ask($question, function (Answer $answer) {
            $this->name = $answer->getText();
       
        $image = Image::url('https://ibb.co/ZBCzv1Y')->title('cavalo');
        $message = OutgoingMessage::create('This is a cavalo.')->withAttachment($image);
        if($this->name == "cavalo")
        {
        $this->say($message);
        }
        
        
        $lower = strtolower($this->name);
        
        $nome = $this->tirarAcentos($lower);

        $id = 0;
        $id1 = 0;

        for($i=0; $i <strlen($this->name); $i++)
        {
            $ascii = ord($this->name[$i]) -96;
            $id = $id + $ascii;
            
        }

        for($i=0; $i <strlen($this->name); $i++)
        {
            $ascii = 27-(ord($this->name[$i])-96);
            $id1 = $id1 + $ascii;
        }
        
        
        $moeda = json_decode(@file_get_contents('https://api.coinlore.net/api/ticker/?id='. urlencode($id)));
        $moeda1 = json_decode(@file_get_contents('https://api.coinlore.net/api/ticker/?id='. urlencode($id1)));
        
        if($moeda && $moeda1)
        {
            $price = (floatval($moeda[0]->price_usd) + floatval($moeda1[0]->price_usd))/2;
            $this->say("Sua moeda vale: $".$price);
            $carteira = 100*$price;
            $this->say("você tem na sua carteira: $".$carteira." da moeda ".$this->name.". Que equivalem a 100 unidades");
            $user = $this->bot->getUser();
            $id = $user->getId();
            $username = $user->getUsername();
            $this->bot->userStorage()->save([
                'moeda' => $this->name,
                'preço' => $price,
                'carteira' => $carteira,
            ]);


            $pessoa = Moeda::all()->where('username', '=', $id)->first();
            if($pessoa)
            {
                $pessoa->delete();
            }
            Moeda::create([
                'username' => $id,
                'moeda' => $this->name,
                'preco' => $price,
                'carteira' => $carteira,
            ]);
        }
        else
        {
            $this->say("Não consegui calcular o valor da sua moeda: tente outro nome");
        }
        
        

        });

        
    }

    public function run()
    {
        $this->Pergunta();
    }
}
