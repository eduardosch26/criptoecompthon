<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;

class Conversao extends Conversation
{
    public function conversao()
    {
        $question = Question::create('Quer ver quantos reais vale sua moeda?')
        ->fallback('Nao foi possivel fazer a conversao')
        ->callbackId('create_conversao')
        ->addButtons([
            Button::create('Sim')->value('yes'),
            Button::create('Não')->value('no'),
        ]);

        $selectedValue = 0;
    return $this->ask($question, function (Answer $answer) {
        // Detect if button was clicked:
        if ($answer->isInteractiveMessageReply()) {
            $selectedValue = $answer->getValue(); // will be either 'yes' or 'no'
        }
    

    if($selectedValue == "yes")
    {
        $real = json_decode(@file_get_contents('https://economia.awesomeapi.com.br/json/daily/USD-BRL/'.urlencode(1)));
        $user = $this->bot->getUser();
        $id = $user->getId();
        $userinformation = $this->bot->userStorage()->find($id);
        $element = $userinformation->get('preço');
        $element = $element*$real[0]->high;
        $this->say("R$".$element);

        
    }
    else
    {
        $this->say("Tudo bem :(");
    }


    });

    } 


    public function run()
    {
        $this->conversao();
    }
}