<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use App\Moeda;
class Comprar extends Conversation
{
    protected $moedinha, $sellerWallet, $seller_id;
    public function Comprar()
    {
        $moeda = Moeda::all()->pluck('moeda');
        $array = [];

        foreach($moeda as $coin)
        {
            array_push($array, Button::create($coin)->value($coin));
            
        }

        $question = Question::create("Escolha a moeda que você quer comprar: ")
        ->fallback('Nao foi possivel fazer a conversao')
        ->callbackId('create_conversao')
        ->addButtons(
            $array
        );

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $selectedValue = $answer->getValue();
            }
            $user = (Moeda::all()->where('moeda', '=', $selectedValue)->first());
            $this->moedinha = ($user->preco);
            $this->sellerWallet = ($user->carteira);
            $this->seller_id = ($user->id);

            $question = Question::create("Digite o numero de moedas que voce quer comprar: ");

            return $this->ask($question, function (Answer $answer) {
                $this->value = $answer->getText();

               
                $value = $this->value;
                $soma = ($this->moedinha)*($value);
                $user = $this->bot->getUser();
                $id = $user->getId();
                $userinformation = $this->bot->userStorage()->find($id);
                $buyerWallet = $userinformation->get('carteira');
                $buyerCoin = $userinformation->get('moeda');
                $buyer = (Moeda::all()->where('moeda', '=', $buyerCoin)->first());
                $buyerWallet = $buyer->carteira;
                $buyer_id = $buyer->id;

                if($buyer_id == $this->seller_id)
                {
                    $this->say("você está tentando comprar sua propria moeda!");
                }
                else if($soma > $buyerWallet)
                {
                    $this->say("Você não tem dinheiro o suficiente na carteira para comprar: ".$this->value." Unidade/s");
                }
                else
                {
                    $buyerWallet = $buyerWallet - $soma;
                    $seller = Moeda::find($this->seller_id);
                    $buyer = Moeda::find($buyer_id);

                    $buyer->carteira = $buyerWallet;

                    $carteira = $this->sellerWallet;
                    $this->sellerWallet =  $carteira+$soma;

                    $seller->carteira = $this->sellerWallet;

                    $seller->save();
                    $buyer->save();
                    $this->say("Compra Realizada com sucesso!");
                }
            });
        });

 
    
    }
    public function run()
    {
        $this->comprar();
    }

    
}