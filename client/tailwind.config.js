module.exports = {
  purge: [
    './components/**/*.{vue,js}',
     './layouts/**/*.vue',
     './pages/**/*.vue',
     './plugins/**/*.{js,ts}',
     './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors : {
        'gray-dark' : '#181717',
        'gray-black': '#202020',
      },
      transitionProperty: {
        'height': 'height'
      },
      spacing: {
        '100': '28rem',
        '114': '34rem',
      }
    },
  },
  variants: {
    extend: {
      height: ['responsive', 'hover', 'focus'],
    },
  },
  plugins: [],
}
